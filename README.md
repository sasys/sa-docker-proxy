# nginx Configuration And Docker Build For Project SA

## Notes

Within the docker network (network named sa) hosts are looked up by their name followed by the network name.  So, if COuchdb is deployed on docker name razor, the network lookup (hostname) with the docker network is razor.sa.

Thus, the network names must be used consistently.

### Network Names With The Docker Network sa

|Image|Running Image Name|Network Name|
|---|---|---|
|Couchdb|razor|razor.sa|
|sa-server|sa-server|sa-server.sa|
|my-rust-app (TBChanged!!!)| sa-md|sa-md.sa|

## Create And Run Docker Image

```
docker build -t sa-nginx-proxy .

docker run --name sa-nginx-proxy -d -p 80:80 --network=sa sa-nginx-proxy
```

## Configuration Notes

All exposed web services are assumed to be on port 80, this is what the web client expects.