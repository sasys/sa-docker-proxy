#
# These represent the mappings of urls to services mapped by paths.
# This proxy presents all services on port 80.
# docker logs
events {
  worker_connections  1024;  ## Default: 1024
}
http {
    log_format compression '$remote_addr - $remote_user [$time_local] '
                        '"$request" $status $body_bytes_sent '
                        '"$http_referer" "$http_user_agent" "$gzip_ratio"';
    server {
        listen 80;
        server_name sa.softwarebynumbers.com store.sa.softwarebynumbers.com;
        access_log /var/log/nginx-access.log compression;
        error_log /var/log/error.log warn;

        #
        # Note, the order matters here, most specific to most general
        #
        # The Rust sa-md service
        #
        location /api/model/check/status {
            proxy_pass http://sa-md.sa:8000;
        }

        #
        # The application version for the given model
        #
        location /api/app-version {
            proxy_pass http://sa-md.sa:8000;
        }

        #
        # The model version for the given model
        #
        location /api/model-version {
            proxy_pass http://sa-md.sa:8000;
        }




        #
        # Given model json, parse it out to md
        # See https://enable-cors.org/server_nginx.html for CORS info
        #
        location /api/transform/modeltomd {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                #
                # Custom headers and headers various browsers *should* be OK with but aren't
                #
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                #
                # Tell client that this pre-flight info is valid for 20 days
                #
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            if ($request_method = 'POST') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
                proxy_pass http://sa-md.sa:8000;
            }
        }
        #
        # Given model as markdown, parse it out to a json string
        # See https://enable-cors.org/server_nginx.html for CORS info
        #
        location /api/transform/mdtomodel {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                #
                # Custom headers and headers various browsers *should* be OK with but aren't
                #
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                #
                # Tell client that this pre-flight info is valid for 20 days
                #
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            if ($request_method = 'POST') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
                proxy_pass http://sa-md.sa:8000;
            }
        }
        #
        # The Clojure model service
        #
        location /api/model {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, PUT, DELETE, OPTIONS';
                add_header 'Access-Control-Allow-Credentials' 'true';
                add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            if ($request_method = 'POST') {
                add_header 'Access-Control-Allow-Credentials' 'true';
                add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
                proxy_pass http://sa-server.sa:3001;
            }
            if ($request_method = 'GET') {
                #HAD TO REMOVE THE Access-Control thing here becuase it produced an error with more than one access control element!!
                # SAME FOR PUT etc
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                proxy_pass http://sa-server.sa:3001;
            }
            if ($request_method = 'PUT') {
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
                proxy_pass http://sa-server.sa:3001;
            }
            if ($request_method = 'DELETE') {
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
                proxy_pass http://sa-server.sa:3001;
            }
        }

        #
        # The Clojure model service for format conversions
        #
        location /api/jsontotransit {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                #
                # Custom headers and headers various browsers *should* be OK with but aren't
                #
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                #
                # Tell client that this pre-flight info is valid for 20 days
                #
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            if ($request_method = 'POST') {
                #add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
                proxy_pass http://sa-server.sa:3000;
            }
        }
        location /api/transittojson {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                #
                # Custom headers and headers various browsers *should* be OK with but aren't
                #
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                #
                # Tell client that this pre-flight info is valid for 20 days
                #
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            if ($request_method = 'POST') {
                #add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
                proxy_pass http://sa-server.sa:3000;
            }
        }


        #
        # Keycloak
        #
        #  http://sa.softwarebynumbers.com:8080/auth/realms/sa-teams/protocol/openid-connect/token. (Reason: CORS header 'Access-Control-Allow-Origin' missing).
        #
        # location /auth {
        #     add_header 'Access-Control-Allow-Origin' 'http://sa.softwarebynumbers.com:3000';
        #     add_header 'Access-Control-Allow-Methods' 'GET, POST, PUT, OPTIONS';
        #     proxy_pass http://keycloak.sa:8080;
        # }

        #
        # The Couch db
        #
        location /_utils {
            proxy_pass http://razor.sa:5984;
        }
        location /_session {
            proxy_pass http://razor.sa:5984;
        }
        location /_all_dbs {
            proxy_pass http://razor.sa:5984;
        }
        location /sa/_all_docs {
            proxy_pass http://razor.sa:5984;
        }
        location /sadebug/_all_docs {
            proxy_pass http://razor.sa:5984;
        }
        location /_replicator {
            proxy_pass http://razor.sa:5984;
        }
        location /_uuids {
            proxy_pass http://razor.sa:5984;
        }
        location /_membership {
            proxy_pass http://razor.sa:5984;
        }
        location /_cluster_setup {
            proxy_pass http://razor.sa:5984;
        }
        location /sa {
            proxy_pass http://razor.sa:5984;
        }
        location /sadebug {
            proxy_pass http://razor.sa:5984;
        }
    }
}
